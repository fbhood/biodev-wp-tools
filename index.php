<?php

/*
Plugin Name:  faBio Development tools plugin 
Description:  A container for development plugins including Developer, Query Monitor and more 
Version:      2.0.0
Contributor:  fabsere
Contributor URI:   https://fabiopacifici.com/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  biodev
Domain Path:  /languages
*/

define('BIODEV_PLUGIN_URL', __FILE__);

/* TODO: check the plugins versions if a new version is available then replace the outdated version in the 
 developer-addons folder */
//wp_update_plugins();


////////////////////////////////////////////////////
// Plugins: Developer + Addons
///////////////////////////////////////////////////


/* Developer + Addons */

// Include the main Developer plugin 
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer/developer.php');


//Include the debug-bar
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar/debug-bar.php');


// Include debug-bar-console
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-console/debug-bar-console.php');

//Include debug-bar-constants
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-constants/debug-bar-constants.php');

// Include debug-bar-cron
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-cron/debug-bar-cron.php');

//debug-bar-extender
//include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-extender/debug-bar-extender.php');

//debug-bar-list-dependencies
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-list-dependencies/debug-bar-list-dependencies.php');

//debug-bar-post-types
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-post-types/debug-bar-post-types.php');

//debug-bar-remote-requests
//include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-remote-requests/debug-bar-remote-requests.php');

//debug-bar-shortcodes
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-shortcodes/debug-bar-shortcodes.php');

//debug-bar-transients
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-transients/debug-bar-transients.php');

//log-deprecated-notices
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/log-deprecated-notices/log-deprecated-notices.php');

//log-viewer
//include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/log-viewer/log-viewer.php');

//monster-widget
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/monster-widget/monster-widget.php');

//query-monitor
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/query-monitor/query-monitor.php');

//rewrite-rules-inspector
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/rewrite-rules-inspector/rewrite-rules-inspector.php');

//wordpress-beta-tester
//include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/wordpress-beta-tester/wp-beta-tester.php');

//Include debug-bar-actions-and-filters-addon
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-actions-and-filters-addon/debug-bar-action-and-filters-addon.php');

// Include Theme-check
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/theme-check/theme-check.php');

// Include Theme-sniffer
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/theme-sniffer/theme-sniffer.php');

// Include Regenerate Thumbnails

include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/regenerate-thumbnails/regenerate-thumbnails.php');


// Include Rewrite rules inspector

include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/rewrite-rules-inspector/rewrite-rules-inspector.php');


// Include rtl-tester
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/rtl-tester/rtl-tester.php');

// Include user-switching
include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/user-switching/user-switching.php');
