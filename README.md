#faBio Development tools
Developer / Debug-bar + addons / Query Monitor + addons /

- A set of must have plugins to help wordpress developers, Develope and debug.
- This bundle of plugins has unique scope to keep the plugin page in the backend tidy and organised while developing themes or plugins.

#List of plugins included for development purposes:

    // Include the main Developer plugin
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer/developer.php');

    //Include the debug-bar
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar/debug-bar.php');


    // Include debug-bar-console
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-console/debug-bar-console.php');

    //Include debug-bar-constants
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-constants/debug-bar-constants.php');

    // Include debug-bar-cron
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-cron/debug-bar-cron.php');

    //debug-bar-extender
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-extender/debug-bar-extender.php');

    //debug-bar-list-dependencies
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-list-dependencies/debug-bar-list-dependencies.php');

    //debug-bar-post-types
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-post-types/debug-bar-post-types.php');

    //debug-bar-remote-requests
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-remote-requests/debug-bar-remote-requests.php');

    //debug-bar-shortcodes
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-shortcodes/debug-bar-shortcodes.php');

    //debug-bar-transients
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-transients/debug-bar-transients.php');

    //log-deprecated-nitices
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/log-deprecated-notices/log-deprecated-notices.php');

    //log-viewer
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/log-viewer/log-viewer.php');

    //monster-widget
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/monster-widget/monster-widget.php');

    //query-monitor
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/query-monitor/query-monitor.php');

    //rewrite-rules-inspector
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/rewrite-rules-inspector/rewrite-rules-inspector.php');

    //wordpress-beta-tester
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/wordpress-beta-tester/wp-beta-tester.php');

    //Include debug-bar-actions-and-filters-addon
    include_once(dirname(BIODEV_PLUGIN_URL) . '/developer-addons/debug-bar-actions-and-filters-addon/debug-bar-action-and-filters-addon.php');
